import React, {Component} from 'react';
import { ScrollView, View, Text, TextInput, Keyboard, Dimensions, Platform, InteractionManager  } from 'react-native';

const {height, width} = Dimensions.get('window');
export default class TestFormScreen extends Component {
    state = {
        height: height
    }
    componentWillMount() {
        if(Platform.OS === 'ios') {
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardWillHide', 
                this.keyboardDidHide.bind(this)
              )
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardWillShow', 
                this.keyboardDidShow.bind(this)
              )
        } else {
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidHide', 
                this.keyboardDidHide.bind(this)
              )
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidShow', 
                this.keyboardDidShow.bind(this)
              )
        }
    }    
    
    componentWillUnmount() {
        this.keyboardDidHideListener.remove()
    }
    
    keyboardDidHide(e) {
        console.log('hello')
        const self = this;
        
        self._scroll.scrollTo({x: 0, y: 0, animated: true}); 
    }

    keyboardDidShow(e) {
        console.log('hello')
        const self = this;
        this.setState({height: height - e.endCoordinates.height});
        self._scroll.scrollTo({x: 0, y: 0, animated: true}); 
    }

    onFocus = (ref) => {
        const self = this;
        
        console.log(this.state.height);
        InteractionManager.runAfterInteractions(() => {
            this.refs[ref].measure((oy, ox, width, height, px, py) => {
                console.log( self.state.height - 40,self.state.height )
                if( py > self.state.height ) {                
                    self._scroll.scrollTo({x: 0, y: (self.state.height - (py - self.state.height)) / 1.5, animated: true});
                }
            })
        })
        
    }

    render() {
        return (
            <View style={{ flex: 1, marginTop: 40 }}>
                <Text>{this.state.height}</Text>
                <ScrollView ref={viewToLift => this._scroll = viewToLift} style={{ flex: 1, backgroundColor: 'pink'}} showsVerticalScrollIndicator={false} scrollEnabled={false}> 
                    <TextInput ref="a" onFocus={() => this.onFocus('a')} style={{opacity: 1, height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="b" onFocus={() => this.onFocus('b')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="c" onFocus={() => this.onFocus('c')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput> 
                    <TextInput ref="d" onFocus={() => this.onFocus('d')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="e" onFocus={() => this.onFocus('e')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="f" onFocus={() => this.onFocus('f')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput> 
                    <TextInput ref="g" onFocus={() => this.onFocus('g')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="h" onFocus={() => this.onFocus('h')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="i" onFocus={() => this.onFocus('i')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput> 
                    <TextInput ref="j" onFocus={() => this.onFocus('j')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="almost" onFocus={() => this.onFocus('almost')} style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <TextInput ref="last" onFocus={() => this.onFocus('last')}  style={{height: 40, width: 100, backgroundColor: '#ccc', marginBottom: 10 }}></TextInput>
                    <View style={{ marginBottom: height + 200}} />
                </ScrollView>
            </View>
        )
    }
}